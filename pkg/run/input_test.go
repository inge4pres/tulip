package run

import (
	"github.com/augustoroman/v8"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

const (
	jsInput = `let d = ` + ctxVisibleName + `.map_data; d`
)

func Test_anInputIsFilledWithMapData(t *testing.T) {
	i := NewContext([]byte(testJSON))
	assert.JSONEq(t, testJSON, string(i.data.RawData))
	assert.Equal(t, "test1", i.data.MapData["first"].(string))
}

func Test_contextBindingToValues(t *testing.T) {
	js := NewJavascript(ctxVisibleName+".map_data;", "empty.js")
	// leaving this to recover in the future
	//
	//v, err := js.isolate().context.Global().Get(ctxVisibleName)
	//if err != nil {
	//	t.Fatalf("error getting map_data from function isolate: %v", err)
	//}
	//log.Println("ctx ",v.String())

	//mappedData, err := v.Get("map_data")
	//if err != nil {
	//	t.Fatalf("could not get map_data from input: %v", err)
	//}
	//result, err := mappedData.MarshalJSON()
	//
	//end
	result, err := js.Run(NewContext([]byte(testJSON)))
	assert.Nil(t, err)
	assert.JSONEq(t, testJSON, string(result), "input and output JSON are different")
}

func Test_singleInputCanBeFedToFunction(t *testing.T) {
	js := NewJavascript(jsInput, "input_from_ctx.js")
	result, err := js.Run(NewContext([]byte(testJSON)))
	assert.Nil(t, err)
	assert.JSONEq(t, testJSON, string(result))
}

func Test_mapInput(t *testing.T) {
	type args struct {
		d []byte
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]interface{}
		wantErr bool
	}{
		{"with_json", args{[]byte(testJSON)}, map[string]interface{}{
			"first":  "test1",
			"second": "test2",
		}, false},
		{"with_array", args{[]byte(`["one","two"]`)}, nil, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := mapInput(tt.args.d)
			if (err != nil) != tt.wantErr {
				t.Errorf("mapInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mapInput() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewContext(t *testing.T) {
	type args struct {
		d []byte
	}
	tests := []struct {
		name string
		args args
		want *Context
	}{
		{name: "with_json", args: args{[]byte(testJSON)}, want: &Context{
			data: Input{
				RawData: []byte(testJSON),
				MapData: map[string]interface{}{
					"first":  "test1",
					"second": "test2",
				},
			},
			helpers: standardHelpers(),
			context: v8.NewIsolate().NewContext()},
		},
		{name: "with_array", args: args{[]byte(`["one","two"]`)}, want: &Context{
			data: Input{
				RawData: []byte(`["one","two"]`),
				MapData: nil,
			},
			helpers: standardHelpers(),
			context: v8.NewIsolate().NewContext()},
		},
		{name: "with_nil", args: args{nil}, want: &Context{
			data: Input{
				RawData: nil,
				MapData: nil,
			},
			helpers: standardHelpers(),
			context: v8.NewIsolate().NewContext()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewContext(tt.args.d); !reflect.DeepEqual(got.data, tt.want.data) {
				t.Errorf("NewContext() = %v, want %v", got, tt.want)
			}
		})
	}
}
