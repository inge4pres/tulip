package run

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
)

// Function describes the possible behaviors of a function:
// running, the source code and its name
type Function interface {
	Run(*Context) ([]byte, error)
	Code() string
	Name() string
}

// Javascript represents a JS function with source code in string form
type Javascript struct {
	code string
	name string
	ctx  *Context
}

// NewJavascript creates a new Javascript function
func NewJavascript(code, name string) *Javascript {
	return &Javascript{
		code: code,
		name: name,
	}
}

// Code implements Function interface
func (js *Javascript) Code() string {
	return js.code
}

// Name implements Function interface
func (js *Javascript) Name() string {
	return js.name
}

// Run implements Function interface
func (js *Javascript) Run(ctx *Context) ([]byte, error) {
	if ctx == nil {
		ctx = NewContext(nil)
	}
	if err := ctx.bind(); err != nil {
		return nil, err
	}
	return runContext(js, ctx)
}

// JsFile is a Javascript file
type JsFile struct {
	code string
	name string
	ctx  *Context
}

// Code implements Function interface
func (js *JsFile) Code() string {
	return js.code
}

// Name implements Function interface
func (js *JsFile) Name() string {
	return js.name
}

// Run implements Function interface
func (js *JsFile) Run(ctx *Context) ([]byte, error) {
	if ctx == nil {
		ctx = NewContext(nil)
	}
	if err := ctx.bind(); err != nil {
		return nil, err
	}
	return runContext(js, ctx)
}

// NewJsFile creates a ne JsFile from a reader
// will return an error if the reader can't be read
func NewJsFile(r io.Reader, name string) (*JsFile, error) {
	c, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("could not create JsFile from reader %v: %v", r, err)
	}
	return &JsFile{
		code: string(c),
		name: name,
		ctx:  nil,
	}, nil
}

func runContext(js Function, ctx *Context) ([]byte, error) {
	result, err := ctx.context.Eval(js.Code(), js.Name())
	if err != nil {
		return nil, err
	}
	bytesResp := result.Bytes()
	if len(bytesResp) > 0 {
		return bytesResp, nil
	}
	return result.MarshalJSON()
}

// Sequence will run a sequence of functions passing the context and will collect
// ether the response if the execution was successful or the error for eveyr function.
// The resulting payload will be a dictionary with the name of the function as key
// and its execution result as value
func Sequence(ctx *Context, fns ...Function) ([]byte, error) {
	// requires json.RawMessage, but why?
	ret := make(map[string]json.RawMessage)
	var exErr error
	for _, f := range fns {
		resp, err := f.Run(ctx)
		if err != nil {
			ret[f.Name()] = []byte(fmt.Sprintf(`{"error": "%v"}`, err))
			exErr = errors.New(exErr.Error() + err.Error())
			continue
		}
		ret[f.Name()] = resp
	}
	if exErr != nil {
		return nil, exErr
	}
	r, err := json.Marshal(&ret)
	if err != nil {
		return []byte(fmt.Sprintf("%s", ret)), err
	}
	return r, nil
}
