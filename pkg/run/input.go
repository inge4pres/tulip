package run

import (
	"encoding/json"
	"github.com/augustoroman/v8"
)

const ctxVisibleName = "context"

// Context wraps the V8 context and adds some data to be passed to functions
type Context struct {
	context *v8.Context
	helpers []v8.Callback
	data    Input
}

// Input is data fed to the function at runtime
// Both the raw data and a map of data will be passed to the function
// so they can be used as an ArrayBuffer or accessed as obect
type Input struct {
	RawData []byte                 `v8:"arraybuffer" json:"raw_data"`
	MapData map[string]interface{} `json:"map_data"`
}

func mapInput(d []byte) (map[string]interface{}, error) {
	ret := make(map[string]interface{}, 0)
	if err := json.Unmarshal(d, &ret); err != nil {
		return nil, err
	}
	return ret, nil
}

// NewContext creates a context from data
func NewContext(d []byte) *Context {
	mapped, err := mapInput(d)
	if err != nil {
		mapped = nil
	}
	return &Context{
		context: v8.NewIsolate().NewContext(),
		helpers: standardHelpers(),
		data: Input{
			RawData: d,
			MapData: mapped,
		},
	}
}

func (c *Context) bind() error {
	value, err := c.context.Create(c.data)
	if err != nil {
		return err
	}
	return c.context.Global().Set(ctxVisibleName, value)
}

// for later use
func standardHelpers() []v8.Callback {
	return nil
}
