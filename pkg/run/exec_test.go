package run

import (
	"bytes"
	"github.com/augustoroman/v8"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	testJSON = `{"first": "test1", "second": "test2"}`

	jsFun = `const multiply = (x,y)=>{ return x*y };
            function execMultiply(first, second){
               return multiply(first, second);
            };
			execMultiply(10, 10);`

	jsObj = `const enrich = (f,s)=>{ return {first: f, second: s}};
			enrich("test1", "test2");`

	jsReadCtx = `var getVal = (o)=>{ return { myObj: o}};
				getVal(context.map_data);`

	jsParseRequestBody = `let bytesView = new Uint8Array(context); bytesView`
)

func Test_v8RunsInHandlerNoContext(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		result, err := v8.NewIsolate().NewContext().Eval(`["one","two"].reverse()`, "handler.js")
		if err != nil {
			t.Fatalf("error: %v", err)
		}
		w.WriteHeader(http.StatusOK)
		response, _ := result.MarshalJSON()
		w.Write(response)
	})

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://any.tld/test", nil)

	h.ServeHTTP(w, r)

	assert.Equal(t, string(w.Body.Bytes()), `["two","one"]`)
	assert.NotEqual(t, string(w.Body.Bytes()), `["one","two"]`)
}

func Test_v8RunsInHandlerWithContext(t *testing.T) {
	h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		context := v8.NewIsolate().NewContext()
		v, err := context.Create(map[string]interface{}{
			"map_data": []string{"one", "two"},
		})
		if err != nil {
			t.Fatalf("could not create context: %v", err)
		}
		if err := context.Global().Set("context", v); err != nil {
			t.Fatalf("error setting context: %v", err)
		}
		result, err := context.Eval(`context.map_data.reverse()`, "handler.js")
		if err != nil {
			t.Fatalf("error: %v", err)
		}
		w.WriteHeader(http.StatusOK)
		response, _ := result.MarshalJSON()
		w.Write(response)
	})

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://any.tld/test", nil)

	h.ServeHTTP(w, r)

	assert.Equal(t, string(w.Body.Bytes()), `["two","one"]`)
	assert.NotEqual(t, string(w.Body.Bytes()), `["one","two"]`)
}

type handler struct {
	ctx *v8.Context
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	v, _ := h.ctx.Create(body)
	_ = h.ctx.Global().Set("context", v)
	result, err := h.ctx.Eval(jsParseRequestBody, "handler.js") //WORKS!
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
	response := result.Bytes()
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}

func Test_v8RunsCustomHandler(t *testing.T) {

	testInput := `{"data": ["one","two"]}`
	buf := bytes.NewBufferString(testInput)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodGet, "http://any.tld/test", buf)

	h := &handler{
		ctx: v8.NewIsolate().NewContext(),
	}

	h.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.Equal(t, string(w.Body.Bytes()), testInput)
}

func Test_JavascriptRun(t *testing.T) {
	js := NewJavascript(jsFun, "run_string.js")
	r, err := js.Run(nil)
	assert.Nil(t, err)
	assert.Contains(t, string(r), "100")
}

func Test_JsFileRun(t *testing.T) {
	buf := bytes.NewBufferString(jsFun)
	js, err := NewJsFile(buf, "run_file.js")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	r, err := js.Run(nil)
	assert.Nil(t, err)
	assert.Contains(t, string(r), "100")
}

func Test_JavascriptReturnsObject(t *testing.T) {
	js := NewJavascript(jsObj, "return_object.js")
	r, err := js.Run(nil)
	assert.Nil(t, err)
	assert.JSONEq(t, testJSON, string(r))
}

func Test_multipleJavascriptShareContext(t *testing.T) {
	c := NewContext([]byte(testJSON))

	js := NewJavascript(jsReadCtx, "first_shared_ctx.js")
	firstResult, err := js.Run(c)
	assert.Nil(t, err, "error running first function: %v", err)

	js2 := NewJavascript(jsReadCtx, "second_shared_ctx")
	secondResult, err := js2.Run(c)
	assert.Nil(t, err, "error running second function: %v", err)

	assert.JSONEq(t, string(secondResult), string(firstResult))
}

func Test_runInGoroutine(t *testing.T) {
	resp := make(chan []byte)
	js := NewJavascript(jsInput, "goroutine_ctx.js")
	go func(f Function) {
		r, err := f.Run(NewContext([]byte(testJSON)))
		if err != nil {
			t.Fatalf("error executing function in goroutine: %v", err)
		}
		resp <- r
	}(js)

	r := <-resp
	log.Println("correctly received response ", string(r))
}

func Test_runFunctionsSequenceSuccess(t *testing.T) {
	fn1 := NewJavascript(`var a = {"first":"test1"}; a`, "fn1")
	fn2 := NewJavascript(`var a = {"hello":"world"}; a`, "fn2")

	c := NewContext([]byte(testJSON))

	s, err := Sequence(c, fn1, fn2)
	assert.NoError(t, err)
	assert.JSONEq(t, `{"fn1":{"first":"test1"},"fn2":{"hello":"world"}}`, string(s))
}

func Test_runFunctionsSequenceError(t *testing.T) {
	fn1 := NewJavascript(`var a = {"first":"test1"}; a`, "fn1")
	fn2 := NewJavascript(`var a = {"hello":"world"};`, "fn2")

	c := NewContext([]byte(testJSON))

	s, err := Sequence(c, fn1, fn2)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "error")
	assert.Contains(t, string(s), "undefined")
}
