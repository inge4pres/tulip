package gateway

import (
	"fmt"
	"gitlab.com/inge4pres/tulip/pkg/run"
	"io/ioutil"
	"net/http"
	"path"
	"sync"
)

// Gateway holds the func/URI mapping for HTTP-triggered functions
type Gateway struct {
	mu      sync.RWMutex
	mapping map[string][]run.Function
}

// NewGateway creates a new Gateway to deliver functions on HTTP requests
func NewGateway() *Gateway {
	return &Gateway{
		mu:      sync.RWMutex{},
		mapping: make(map[string][]run.Function, 0),
	}
}

// Add assings a function to the gateway for a given URI
// If the URI is already mapped with a function it will add to the existing one.
// Gateway has a mutex to make the function assignment concurrency-safe
func (g *Gateway) Add(fn run.Function, uri string) error {
	g.mu.Lock()
	g.mapping[uri] = append(g.mapping[uri], fn)
	g.mu.Unlock()
	return nil
}

func (g *Gateway) hasFunc(uri string) ([]run.Function, bool) {
	g.mu.RLock()
	f, ok := g.mapping[path.Clean(uri)]
	g.mu.RUnlock()
	return f, ok
}

// Handler returns the http.Handler that serves the requests for the gateway
func Handler(g *Gateway) http.Handler {
	// TODO differentiate by HTTP method, setup a router or use a library
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		uri := r.URL.Path
		if functions, ok := g.hasFunc(uri); ok {
			b, err := ioutil.ReadAll(r.Body)
			if err != nil {
				http500(w, fmt.Sprintf("error reading HTTP request body: %v", err))
			}
			defer r.Body.Close()
			context := run.NewContext(b)
			resp, err := run.Sequence(context, functions...)
			if err != nil {
				http500(w, fmt.Sprintf("function execution error: %v", err))
				return
			}
			http200(w, resp)
			return
		}
		http404(w)
	})
}

func http500(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, msg)
}

func http200(w http.ResponseWriter, body []byte) {
	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

func http404(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
}
