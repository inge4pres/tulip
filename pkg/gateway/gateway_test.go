package gateway

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/inge4pres/tulip/pkg/run"
)

var (
	anyJSON = `{"test":"true"}`

	writeCtx = run.NewJavascript(`var a = context.map_data; a.anothertest = "false"; a`, "write_ctx")
	readCtx  = run.NewJavascript(`var a = context.map_data; a`, "read_ctx")
)

func setupGateway(t *testing.T, uri string, fn ...run.Function) *Gateway {
	gw := NewGateway()
	for _, f := range fn {
		if err := gw.Add(f, uri); err != nil {
			t.Fatalf("error mapping: %v", err)
		}
	}
	return gw
}

func Test_aFunctionCanBeAddedToRequestPath(t *testing.T) {
	uri := "/test"
	gw := setupGateway(t, uri, run.NewJavascript(`var a = `+anyJSON+`; a`, "echo"))
	_, hasFunc := gw.hasFunc(uri)
	assert.True(t, hasFunc)
}

func Test_aFunctionCanBeMatchedWithTrailingSlash(t *testing.T) {
	uri := "/test"
	gw := setupGateway(t, uri, run.NewJavascript(`var a = `+anyJSON+`; a`, "echo"))
	r := httptest.NewRequest(http.MethodGet, "http://any.tld/test/", nil)
	_, hasFunc := gw.hasFunc(r.URL.Path)
	assert.True(t, hasFunc)
}

func Test_serveHTTPParsingRequestBody(t *testing.T) {
	uri := "/test"
	gw := setupGateway(t, uri, run.NewJavascript(`var a = `+anyJSON+`; a`, "echo"))

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost,
		"http://api.gw/test/",
		bytes.NewBufferString(anyJSON))

	h := Handler(gw)
	h.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.JSONEq(t, `{"echo":`+anyJSON+`}`, w.Body.String())
}

func Test_multipleFunctionsWithContextResponse(t *testing.T) {
	uri := "/test"
	gw := setupGateway(t, uri, writeCtx, readCtx)

	w := httptest.NewRecorder()
	r := httptest.NewRequest(http.MethodPost,
		"http://api.gw/test/",
		bytes.NewBufferString(anyJSON))

	h := Handler(gw)
	h.ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	assert.JSONEq(t, `{"write_ctx": {"test":"true","anothertest":"false"}, "read_ctx": {"test":"true"}}`, w.Body.String())
}
