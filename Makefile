WORKDIR?=$(shell pwd)
VERSION=$(shell cat .semver)

V8_VERSION?=6.3.292.48.1
TEST_ARG?=
## MacOS High Sierra maps to darwin-17, Mojave to darwin-18
PLATFORM?=linux


lint:
	go fmt ./...
	go vet ./...

pkg-test: lint
	go test -race -v ./pkg/... $(TEST_ARG)

test: pkg-test
	go test -race -v ./cmd $(TEST_ARG)

clean:
	go clean -i ./...

build: clean test
	go build -o bin/tulip -ldflags '-w -extldflags "-static"' .

v8:
	cd $(WORKDIR)/v8dist && \
		curl -s https://rubygems.org/downloads/libv8-$(V8_VERSION)-x86_64-$(PLATFORM).gem | tar xv && \
        mkdir -p data && \
        tar -xzf data.tar.gz -C data
	cd vendor/github.com/augustoroman/v8 && \
		rm -f libv8 && ln -s $(WORKDIR)/v8dist/data/vendor/v8/out/x64.release libv8 && \
		rm -f include && ln -s $(WORKDIR)/v8dist/data/vendor/v8/include include

docker: clean test
	docker build -t inge4pres/tulip-gw:v$(VERSION) .
	docker push inge4pres/tulip-gw:v$(VERSION)

.PHONY: clean pkg-test test lint build docker v8