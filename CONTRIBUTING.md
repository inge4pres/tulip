## Starting the conversation
Every bug, feature request or general question should pass through an issue. 

## Dependiencies
We use [`dep`](https://github.com/golang/dep) because the main dependency (augustormaon/v8) does not have a semver tag on Github, hence `go mod` can't apply at current status.

## V8 Runtime
When you first clone the project, it won't build. This is because the V8 bindings depends by a binary distribution of the V8 libs on your system. Running `make v8` will download the proper version of the V8 binary distribution and make the linking in the `vendor` folder.
On MacOS you should run:

* `PLATFORM=darwin-17 make v8` for High Sierra
* `PLATFORM=darwin-18 make v8` for Mojave

as the default Makefile settings are left for building with Linux.

## Running tests
Running `make test` runs both the package tests and the binaries tests, you can prepend a `TEST_ARG="-run Test_NameOfTests"` or any other `go test` argument.
`make pkg-test` only runs the package tests.