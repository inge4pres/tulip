FROM golang:stretch as build

ENV SRCDIR /go/src/gitlab.com/inge4pres/tulip

COPY . ${SRCDIR}

WORKDIR ${SRCDIR}
RUN make v8 && make build

FROM gcr.io/distroless/base

COPY --from=build /go/src/gitlab.com/inge4pres/tulip/bin/tulip /tulip

ENTRYPOINT ["/tulip"]

