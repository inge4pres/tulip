// https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function assignCustomerId(customer) {
  customer.uuid = guid();
  return customer;
}

var customer = {
  name:            "angel",
  surname:         "michelangelo",
  age:             -1,
  preferred_music: "metal"
};

assignCustomerId(customer);