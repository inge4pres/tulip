## Examples
Once you've built the CLI binary you can run the examples.

#### Simple 
```bash
bin/tulip test simple/sayHello.js
```

will yield the ":cool" emoticon.

---

```bash
bin/tulip test simple/customerId.js
```
 
will generate a JSON object with some data.

#### Context
Context is the functions' input that can be fed to it for processing
You can test input of a function by passing the path to a JSON file that will serve as input (JSON format or binary data).

```bash
bin/tulip test test context/fibonacci.js -i context/fibonacci.json
```
 
the `fibonacci.js` code calculates the sum of fibonacci numbers up to a given number. 
The JSON input file binds the upper bound value in a `operand` field, that is consumed by the function via `context.map_data.operand`.  