var upTo = context.map_data.operand || 10;

var fib = function (x) {
    if (x < 2){
      return x;
    }
    return fib(x-1) + fib(x-2);
};

fib(upTo);
