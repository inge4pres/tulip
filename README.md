# Tulip
Independent blossoms of imperfect beauty

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/inge4pres/tulip)](https://goreportcard.com/report/gitlab.com/inge4pres/tulip)

Tulip is a FaaS platform to run code on demand, driven by HTTP requests.

## Why another platform?
Tulip is greatly inspired by Cloudflare workers, where Chroium V8 Isolates are used to execute Javascript functions at the edge. 
The cost and performance for running this code is way better than containers and still better than using Firecracker so this is the main driver for this platform to exist.

## Running locally
You can test the execution of functions locally calling the `test` subcommand:

```bash
tulip test path_to_code.js -i input_file.json
```

with the previous you will get the result of the function on StandardOut, given the input read from the JSON file.

## Credit
This project could not exist without the amazing work of [V8 Team](https://v8.dev/) on the runtime; thanks to [Augusto Roman](https://github.com/augustoroman) for putting together the [V8 bindings for Go](https://github.com/augustoroman/v8).

