// Copyright © 2018 Francesco Gualazzi <fgualazzi@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/inge4pres/tulip/pkg/run"
	"io/ioutil"
	"log"
	"os"
)

var (
	functionFile, inputFile string
)

// testCmd represents the test command
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "Execute a function",
	Long: `Test your functions locally by running them in a V8 isolate
If you want to pass data the function you can use an input from file, ideally a JSON object that will be napped to a map[string]interface{} but also binary will be available in context.raw_data.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := cmd.ValidateArgs(args); err != nil {
			log.Fatalf("missing argument! specify at least one function file")
		}

		f, err := os.Open(args[0])
		if err != nil {
			log.Fatalf("could not read function file: %v", err)
		}
		i := make([]byte, 0)
		if inputFile != "" {
			i, _ = ioutil.ReadFile(inputFile)
		}
		input := run.NewContext(i)
		js, err := run.NewJsFile(f, f.Name())
		if err != nil {
			log.Fatalf("create function error: %v", err)
		}
		result, err := js.Run(input)
		if err != nil {
			log.Fatalf("run function error: %v", err)
		}
		//noinspection GoUnhandledErrorResult
		fmt.Fprintf(os.Stdout, string(result))
	},
}

func init() {
	rootCmd.AddCommand(testCmd)
	testCmd.Flags().StringVarP(&inputFile, "input", "i", "", "input file")
}
